from django.conf.urls.defaults import patterns, url

urlpatterns = patterns('smart_selects.views',
    url(r'^ss/(?P<app>[\w\-]+)/(?P<model>[\w\-]+)/(?P<field>[\w\-]+)/(?P<value>[\w\-]+)/$', 'filterchain', name='chained_filter'),
    url(r'^ss/(?P<field>[\w\-]+)/(?P<value>[\w\-]+)/$', 'generic_filterchain', name='generic_chained_filter'),

    )
